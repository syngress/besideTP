Rails.application.routes.draw do
  # Turn off registration  -> :skip => :registrations
  devise_for :users

  # Ref to /details page via tickets app
  #scope '/tickets' do
  #  get '/search', to: 'tickets#search'
  #end
  resources :tickets do
    get 'details', on: :member
    get 'search', on: :collection
  end

  get '/ads', :controller=>'ads', :action=>'index'
  get '/ads/:id', :controller=>'ads', :action=>'show'
  post '/ads', :controller=>'ads', :action=>'index'
  get '/reservations/', :controller=>'reservations', :action=>'index'
  get '/reservations/:id', :controller=>'reservations', :action=>'show'

end