class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :rname
      t.string :rstatus
      t.datetime :rreservation
      t.integer :rtype
      t.integer :rfloor
      t.integer :rprice
      t.string :rhistory

      t.timestamps
    end
  end
end
