class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.

  # Metoda dostępna z każdego widoku generowanego przez kontorler dziedziczacy z ApplicationController
  helper_method :return_to

  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Authenticate users befor show content, scope: All
  before_action :authenticate_user!
  before_action :set_return_to, except: [:update, :create]

  # Overwriting the sign_out redirect path method
  def after_sign_in_path_for(resource)
    '/tickets'
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer
  end

  # Safe back method
  def return_to
    params[:return_to] || session[:return_to]
  end

  def set_return_to(path = nil)
    path ||= request.referer
    session[:return_to] = path
    if path.blank?
      session[:return_to] = '/tickets'
    else if path = 'users/sign_in'
           session[:return_to] = '/tickets'
         end
    end
  end

end