class AdsController < ApplicationController
  attr_reader :state, :city, :area_code, :time_zone
  before_action :set_ads, only: [:show]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /ads
  def index
    require 'savon'
    rvnumber = params[:rlogform].present? ? params[:rlogform][:rlogcode] : nil
    if rvnumber == nil
    @message = ""
    else
      wsdlurl = 'http://szop.pl.easypack24.net:8080/ReverseReturnWSService?wsdl'
      client = Savon.client(wsdl: wsdlurl,
                            log: true, # set true to switch on logging
                            log_level: :debug,
                            pretty_print_xml: true)
      giveitback = client.call(:get_report, message: {"code"=>rvnumber})
      testcode = giveitback.to_hash[:get_report_response][:return]
      trycode = testcode[:count]
      if trycode == '0'
             @result = "Mam niestety 0"
        else if trycode > '1'
             @result = "Jestem pusty :)"
           else
             data = giveitback.to_hash[:get_report_response][:return][:result][:target_address]
             @companyname = data[:company_name]
             @province = data[:province]
             @street = data[:street]
             @town = data[:town]
           end
        end
    end
  end

  # GET /ads/1
  def show

  end

  # GET /ads/search
  def search

  end

  # GET /ads/1/edit
  def edit

  end

  # POST /ads
  def create

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_ad

  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def ad_params

  end
end