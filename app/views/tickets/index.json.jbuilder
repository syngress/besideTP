json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :project_name, :ticket_no, :ticket_label, :deploy_date, :status, :email_address
  json.url ticket_url(ticket, format: :json)
end
